# is-it-up-webpage

## What is this?
This python script will load a yaml file containing a list of "endpoints" which it will try to ping, then generate a simple html page that lists which entries respond.

To do the ICMP pings, this must be run as root, so be careful!

## Usage
This software is designed to have as little hard-coding as possible and take advanage of DNS to resolve the ip address.

### Endpoints
Each endpoint is added to the endpoints tag.  This is all that is needed if the device you want to check has a DNS entry of NAME in the DNS server's search domain.

Ex:
```yaml
endpoints:
  apollo01:
```
Will check the equivalent of "ping apollo01"

If you want separate names and DNS entry names, you can do the following
Ex:
```yaml
endpoints:
  apollo01:
    dns_name: not_apollo01
```
This will display the entry apollo01, but ping not_apollo01

If you just want to specify the IP address, do the following

Ex:
```yaml
endpoints:
  apollo01:
    ip: 192.168.1.1
```
This will ping 192.168.1.1 with no DNS checking

####
By default, the "type" of each entry is "other" and they will be grouped in the table "other" in the webpage
You can specify a type for any of the endpoints and that will cause a new table to be generated for that "type" in the webpage
Ex:
```yaml
endpoints:
  apollo01:
    type: blade
```
This will cause apollo01's status to be displayed in the "blade" table instead of the "other" table. 



### DNS
To override the hosts DNS server and use a different one (local network DNS server)
Add the following yaml
```yaml
dns:
  server:
    - "IP"
  search_domain:
    - "example.com"
```
Both Server and search_domain are optional.   They are lists so you can use multiple search_domains and DNS servers




## Prereqs
 - python3
 - pyyaml
 - pyping
 - dnspython
