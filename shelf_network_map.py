#!/bin/env python3

import yaml
from pythonping import ping
import dns.resolver

# Import datetime class from datetime module
from datetime import datetime

def check_ip(ip_address):
    responses = ping(ip_address,count=1,timeout=0.5)
    alive = False
    for response in responses:
        alive = alive or response.success
    return alive

def main(yaml_file,html_file):    
    with open(yaml_file,'r') as f1:
        config = yaml.safe_load(f1)

    #############################################################################
    #setup dns server
    #############################################################################
    dns_server = dns.resolver.Resolver()
    #handle DNS server mods
    if config.get("dns") != None:
        #override the system's dns server with this server
        if config["dns"].get("server") != None:
            dns_server.nameservers = config["dns"]["server"]
        #override the systems search domain with this one
        if config["dns"].get("search_domain"):
            search_list = []
            for value in config["dns"]["search_domain"]:
                search_list.append(dns.name.from_text(value))
            dns_server.search = search_list

    #############################################################################
    #Find all listed endpoints and say if they are up or not
    #############################################################################
    organized_endpoints = dict()
    for name in config["endpoints"]:
        endpoint=config["endpoints"][name]
        
        #check if we've specified the ip address
        if endpoint.get("ip") == None:
            #we don't have an explicit ip address, reslove it with the name server
            #the default name is the endpoints's name            
            resolve_name=name
            #check if we have a dns_name
            if endpoint.get("dns_name") != None:
                #we have a dns name, resolve it
                resolve_name= endpoint["dns_name"]

            #resolve the name (the loop is because this returns a list)
            #if there are multiple resolved ips, use the last
            query_results = dns_server.query(resolve_name)
            for ip_address in query_results:
                ip=str(ip_address)
        else:
            #we have an ip address
            ip= endpoint["ip"]

        #check that we've ended up with an ip
        if ip == None:
            continue

        #check if this address is up
        if check_ip(ip):
            is_up = "yes"
        else:            
            is_up = "no"

        if endpoint.get("type") != None:
            endpoint_type=endpoint["type"]
        else:
            endpoint_type="Other"

        #add this endpoint to the organized_endpoints data structure
        if organized_endpoints.get(endpoint_type) == None:
            organized_endpoints[endpoint_type]=[]
        organized_endpoints[endpoint_type].append(
            {
                "type": endpoint_type,
                "name": name,
                "ip"  : ip,
                "is_up" : is_up
            }
        )

    with open(html_file,'w') as out:
        out.write("<!DOCTYPE html>")
        out.write("<html>")
        out.write("<style>")
        out.write("table {float: left; margin: 10px}")
        out.write("th { font-size: smaller; background-color: lightblue;}")
        out.write("th.name {font-size: 20px; }")
        out.write("td { background-color: lightgrey; text-align: right;}")
        out.write("td.nonerror { background-color: lightgreen;}")
        out.write("td.warning { background-color: #FFFF00;}")
        out.write("td.error { background-color:#FB412d;}")
        out.write("td.null { background-color: lightgrey;}")
        out.write("</style>")
        out.write("<body>")

        out.write("<p> Updated @ "+str(datetime.now())+"</p>")
        
        for endpoint_type in organized_endpoints:
            out.write("<table>")
            out.write("  <tr>")
            out.write("    <th>"+endpoint_type+"</th>")
            out.write("    <th>IP</th>")
            out.write("    <th>ping?</th>")
            out.write("  </tr>")
            for endpoint in organized_endpoints[endpoint_type]:
                out.write("<tr>")
                
                out.write("  <td class=\"name\">")
                out.write(endpoint["name"])
                out.write("</td>")

                out.write("  <td class=\"nonerror\">")
                out.write(endpoint["ip"])
                out.write("</td>")

                if endpoint["is_up"] == "yes":
                    out.write("  <td class=\"nonerror\">")
                else:
                    out.write("  <td class=\"error\">")
                out.write(endpoint["is_up"])
                out.write("</td>")

                out.write("</tr>")

        out.write("</body></html>")
        
        
main("config.yaml","test.html")
